import React from "react";
import {connect} from 'react-redux';
import {GameRun, GameSetup} from '../components';
import {initGame, initGameInterface, movePuzzle, movePuzzleInterface, resetGame, resetGameInterface} from '../actions';
import {GameState} from "../reducers/game";


export const Game: React.FC<{ game: GameState, movePuzzle: movePuzzleInterface, initGame: initGameInterface, resetGame: resetGameInterface }> = (props) => {

  const renderSetup = () => (<GameSetup initGame={props.initGame}/>);
  const renderGame = () => (<GameRun game={props.game} movePuzzle={props.movePuzzle} resetGame={props.resetGame}/>);

  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%'
    }}>
      {!props.game.isStart && renderSetup()}
      {props.game.isStart && renderGame()}
    </div>
  )
};

const mapStateToProps = (state: any) => ({
  game: state.game,
});

export default connect(
  mapStateToProps,
  {movePuzzle, initGame, resetGame}
)(Game);

