import {GameState} from "../reducers/game";


/**
 * Puzzles in scenario
 * ┌───────┬───────┬───────┐
 * │ (0,0) │ (1,0) │ (2,0) │
 * │ {id:1}│ {id:3}│ {id:5}│
 * ├───────┼───────┼───────┤
 * │ (0,1) │ (1,1) │ (2,1) │
 * │ {id:2}│ {id:8}│ {id:0}│
 * ├───────┼───────┼───────┤
 * │ (0,2) │ (1,2) │ (2,2) │
 * │ {id:6}│ {id:4}│ {id:7}│
 * └───────┴───────┴───────┘
 */

export const scenario1: GameState = {
  isStart: true,
  isEnd: false,
  size: {
    horizontalCount: 3,
    verticalCount: 3,
    widthPuzzle: 100,
    heightPuzzle: 100,
  },
  puzzles: [
    {id: 0, dx: 0, dy: 0},
    {id: 1, dx: 0, dy: 0},
    {id: 2, dx: 0, dy: 0},
    {id: 3, dx: 0, dy: 0},
    {id: 4, dx: 0, dy: 0},
    {id: 5, dx: 0, dy: 0},
    {id: 6, dx: 0, dy: 0},
    {id: 7, dx: 0, dy: 0},
    {id: 8, dx: 0, dy: 0},
  ],
  structurePuzzles: [1, 3, 5, 2, 8, 0, 6, 4, 7]
};
