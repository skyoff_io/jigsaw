import {GameState} from "../reducers/game";


/**
 * Puzzles in scenario
 * ┌───────┬───────┬───────┐
 * │ (0,0) │ (1,0) │ (2,0) │
 * │ {id:1}│ {id:0}│ {id:2}│
 * └───────┴───────┴───────┘
 */

export const scenario2: GameState = {
  isStart: true,
  isEnd: false,
  size: {
    verticalCount: 1,
    horizontalCount: 3,
    widthPuzzle: 30,
    heightPuzzle: 30,
  },
  puzzles: [
    {id: 0, dx: 0, dy: 0},
    {id: 1, dx: 0, dy: 0},
    {id: 2, dx: 0, dy: 0}
  ],
  structurePuzzles: [1, 0, 2]
};
