
export type point = [number, number]

export type actionState<T> = {
  type: string;
  payload: T;
}

export type puzzle = {
  id: number;
  dx: number;
  dy: number;
};

export type movePuzzleData = {
  from: point,
  to: point,
}

export type gameStatePuzzles = number[];

export type datasetPuzzle = {
  point: point,
  puzzle: puzzle
}

export type loadImageResult = {
  image?: HTMLImageElement,
  error?: string
};
