import {jigsawGenerator} from "./jigsawGenerator";
import {GameState} from "../reducers/game";

jest.mock('./loadImage', () => ({loadImage: jest.fn(() => ({image: {width: 1024, height: 527}}))}));

describe('Helpers', () => {
  describe('Jigsaw generator', () => {
    const puzzleTargetSize = 130;
    const min = 260;
    const max = 1024;
    const url = 'fakeUrlForMock';

    it('should not return an error', async () => {
      expect.assertions(1);
      const gameState = await jigsawGenerator(puzzleTargetSize)(min)(max)(url) as {error?:boolean};
      expect(gameState.error).toBeUndefined();
    });

    it('should return data', async () => {
      expect.assertions(6);
      const gameState = await jigsawGenerator(puzzleTargetSize)(min)(max)(url) as GameState;
      expect(gameState.image).toStrictEqual({height: 527, width: 1024});
      expect(gameState.isEnd).toBe(false);
      expect(gameState.isStart).toBe(true);
      expect(gameState.puzzles).toStrictEqual([
        {
          "dx": 0,
          "dy": 0,
          "id": 0
        },
        {
          "dx": 128,
          "dy": 0,
          "id": 1
        },
        {
          "dx": 256,
          "dy": 0,
          "id": 2
        },
        {
          "dx": 384,
          "dy": 0,
          "id": 3
        },
        {
          "dx": 512,
          "dy": 0,
          "id": 4
        },
        {
          "dx": 640,
          "dy": 0,
          "id": 5
        },
        {
          "dx": 768,
          "dy": 0,
          "id": 6
        },
        {
          "dx": 896,
          "dy": 0,
          "id": 7
        },
        {
          "dx": 0,
          "dy": 105,
          "id": 8
        },
        {
          "dx": 128,
          "dy": 105,
          "id": 9
        },
        {
          "dx": 256,
          "dy": 105,
          "id": 10
        },
        {
          "dx": 384,
          "dy": 105,
          "id": 11
        },
        {
          "dx": 512,
          "dy": 105,
          "id": 12
        },
        {
          "dx": 640,
          "dy": 105,
          "id": 13
        },
        {
          "dx": 768,
          "dy": 105,
          "id": 14
        },
        {
          "dx": 896,
          "dy": 105,
          "id": 15
        },
        {
          "dx": 0,
          "dy": 210,
          "id": 16
        },
        {
          "dx": 128,
          "dy": 210,
          "id": 17
        },
        {
          "dx": 256,
          "dy": 210,
          "id": 18
        },
        {
          "dx": 384,
          "dy": 210,
          "id": 19
        },
        {
          "dx": 512,
          "dy": 210,
          "id": 20
        },
        {
          "dx": 640,
          "dy": 210,
          "id": 21
        },
        {
          "dx": 768,
          "dy": 210,
          "id": 22
        },
        {
          "dx": 896,
          "dy": 210,
          "id": 23
        },
        {
          "dx": 0,
          "dy": 315,
          "id": 24
        },
        {
          "dx": 128,
          "dy": 315,
          "id": 25
        },
        {
          "dx": 256,
          "dy": 315,
          "id": 26
        },
        {
          "dx": 384,
          "dy": 315,
          "id": 27
        },
        {
          "dx": 512,
          "dy": 315,
          "id": 28
        },
        {
          "dx": 640,
          "dy": 315,
          "id": 29
        },
        {
          "dx": 768,
          "dy": 315,
          "id": 30
        },
        {
          "dx": 896,
          "dy": 315,
          "id": 31
        },
        {
          "dx": 0,
          "dy": 420,
          "id": 32
        },
        {
          "dx": 128,
          "dy": 420,
          "id": 33
        },
        {
          "dx": 256,
          "dy": 420,
          "id": 34
        },
        {
          "dx": 384,
          "dy": 420,
          "id": 35
        },
        {
          "dx": 512,
          "dy": 420,
          "id": 36
        },
        {
          "dx": 640,
          "dy": 420,
          "id": 37
        },
        {
          "dx": 768,
          "dy": 420,
          "id": 38
        },
        {
          "dx": 896,
          "dy": 420,
          "id": 39
        }
      ]);
      expect(gameState.size).toStrictEqual({heightPuzzle: 105, horizontalCount: 8, verticalCount: 5, widthPuzzle: 128});
      expect(gameState.structurePuzzles.length).toBe(40);
    });
  });
});
