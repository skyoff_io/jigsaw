export const clone = <T>(a: T): T => JSON.parse(JSON.stringify(a));
