import {GameState} from "../reducers/game";
import {createPuzzleByIndex} from "./puzzles";
import {checkImage} from "./checkImage";
import {loadImage} from "./loadImage";
import {imageRule} from "./imageRule";


function shuffle(a: any) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

const prepareData = (puzzleTargetSize:number) => (image: HTMLImageElement):GameState => {
  const calculatePuzzleSize = (targetSize: number) => (size: number) => Math.floor(size / Math.ceil(size / targetSize));

  const widthPuzzle: number = calculatePuzzleSize(puzzleTargetSize)(image.width);
  const heightPuzzle: number = calculatePuzzleSize(puzzleTargetSize)(image.height);
  const horizontalCount = Math.floor(image.width / widthPuzzle);
  const verticalCount = Math.floor(image.height / heightPuzzle);
  const puzzles = Array(verticalCount * horizontalCount).fill({
    id: 0,
    dx: 0,
    dy: 0
  }).map((element, index) => ({
    ...element, ...createPuzzleByIndex(horizontalCount)({
      width: widthPuzzle,
      height: heightPuzzle
    })(index)
  }));
  const structurePuzzles = shuffle(Array(verticalCount * horizontalCount).fill(0).map<number>((_, index) => index));

  return {
    image,
    puzzles,
    structurePuzzles,
    size: {
      heightPuzzle,
      widthPuzzle,
      verticalCount,
      horizontalCount
    },
    isEnd: false,
    isStart: true,
  };
};

export const jigsawGenerator = (puzzleTargetSize: number) => (minSize: number) => (maxSize:number) => async (url: string): Promise<GameState | {error:string}> => {
  const rules = [
    imageRule.width(minSize)(maxSize),
    imageRule.height(minSize)(maxSize)
  ];
  const imageResult = checkImage(rules)(await loadImage(url));
  return (imageResult.error && {error: imageResult.error}) || prepareData(puzzleTargetSize)(imageResult.image as HTMLImageElement)
};
