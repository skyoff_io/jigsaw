import {imageRule} from "./imageRule";

describe('Helpers', () => {
  describe('Image rule for', () => {
    const min: number = 100;
    const max: number = 200;

    it.each<[{width: number}, any]>([
      [{width: 150}, true],
      [{width: 50}, `The image width must be between ${min}px and ${max}px`],
      [{width: 250}, `The image width must be between ${min}px and ${max}px`],
    ])('width and object %j', function (image, exp) {
      expect(imageRule.width(min)(max)(image)).toStrictEqual(exp)
    });

    it.each<[{height: number}, any]>([
      [{height: 150}, true],
      [{height: 50}, `The image height must be between ${min}px and ${max}px`],
      [{height: 250}, `The image height must be between ${min}px and ${max}px`],
    ])('height and object %j', function (image, exp) {
      expect(imageRule.height(min)(max)(image)).toStrictEqual(exp)
    });
  });
});
