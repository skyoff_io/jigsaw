import {loadImageResult} from "../types";

interface ruleInterface {
  (image: HTMLImageElement): true | string
}

const callRule = (image: loadImageResult) => (rule: ruleInterface) => {
  const result = (image.image && rule(image.image)) || 'Image object does not exist';
  return (result === true && image) || {...image, error: result};
};
export const checkImage = (rules: any[]) => (image: loadImageResult):loadImageResult => rules.reduce((result, rule) => (result.error && result) || callRule(image)(rule), image);
