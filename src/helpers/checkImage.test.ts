import {loadImageResult} from "../types";
import {checkImage} from "./checkImage";

describe('Helpers', () => {
  describe('Check image', () => {
    const rule = jest.fn(image => true);
    const rules = [rule];

    describe('when image have data', () => {
      const loadImage:loadImageResult = {
        image: {width: 100, height: 100} as HTMLImageElement
      };

      const result = checkImage(rules)(loadImage);

      it('should be return object with image', function () {
        expect(result.image).toBe(loadImage.image)
      });

      it('shouldn call the rules', function () {
        expect(rule.mock.calls.length).toBe(1);
      });

      it('should be no error', function () {
        expect(result.error).toBeUndefined();
      });
    });

    describe('when image have error', () => {
      const loadImage:loadImageResult = {
        error: 'error'
      };

      const result = checkImage(rules)(loadImage);

      it('should be return object with error', function () {
        expect(result.error).toBe(loadImage.error)
      });

    });

    describe('when image against the rules', () => {
      const message = 'against the rules';
      const ruleFalse = jest.fn(image => message);
      const loadImage:loadImageResult = {
        image: {} as HTMLImageElement
      };

      const result = checkImage([ruleFalse])(loadImage);

      it('should be return object with error', function () {
        expect(result.error).toBe(message);
      });

      it('shouldn\'t call the rules', function () {
        expect(ruleFalse.mock.calls.length).toBe(1);
      });
    });

    describe('when image object not exist', () => {
      const loadImage:loadImageResult = {};
      const result = checkImage(rules)(loadImage);
      it('should be return object with error', function () {
        expect(result.error).toBe('Image object does not exist');
      });

    });
  });
});
