/**
 * Rule for size
 * @param propertyName
 */
const imageSize = (propertyName: string) => (image: { [key: string]: any }) => (min: number) => (max: number): boolean => image[propertyName] >= min && image[propertyName] <= max;
const imageHeight = imageSize('height');
const imageWidth = imageSize('width');

export const imageRule = {
  height:  (min: number) => (max: number) => (image: { [key: string]: any }) => imageHeight(image)(min)(max) || `The image height must be between ${min}px and ${max}px`,
  width: (min: number) => (max: number) => (image: { [key: string]: any }) => imageWidth(image)(min)(max) || `The image width must be between ${min}px and ${max}px`,
};
