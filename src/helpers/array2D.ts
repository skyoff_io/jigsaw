import {point} from '../types';
import {clone} from "./clone";

const calculateIndex = (itemInRow: number) => (coordinates: point) => coordinates[0] + coordinates[1] * itemInRow;

export const addItem = <T>(itemInRow: number) => (array: T[]) => (coordinates: point) => (item: T) => {
  const newArray = clone<T[]>(array);
  newArray[calculateIndex(itemInRow)(coordinates)] = item;
  return newArray;
};

export const getItem = <T>(itemInRow: number) => (array: T[]) => (coordinates: point): T => array[calculateIndex(itemInRow)(coordinates)];

export const calculateCoordinates = (itemInRow: number) => (index:number): point => [index - Math.floor(index/itemInRow) * itemInRow, Math.floor(index/itemInRow)];
