import {addItem, calculateCoordinates, getItem} from "./array2D";
import {point} from "../types";

describe('Helpers', () => {
  describe('Array 2D', () => {
    describe('Add item on given coordinates', () => {
      it.each<[point, number, any[], any[]]>([
        [[0, 0], 2, ['a', 'b', 'c', 'd'], ['x', 'b', 'c', 'd']],
        [[1, 0], 2, ['a', 'b', 'c', 'd'], ['a', 'x', 'c', 'd']],
        [[0, 1], 2, ['a', 'b', 'c', 'd'], ['a', 'b', 'x', 'd']],
        [[1, 1], 2, ['a', 'b', 'c', 'd'], ['a', 'b', 'c', 'x']],
      ])('%j', function (coordinates, itemInRow, array2D, exp) {
        expect(addItem(itemInRow)(array2D)(coordinates)('x')).toStrictEqual(exp)
      });
    });

    describe('Get item from given coordinates', () => {
      it.each<[point, number, any[]]>([
        [[0, 0], 2, ['x', 'b', 'c', 'd']],
        [[1, 0], 2, ['a', 'x', 'c', 'd']],
        [[0, 1], 2, ['a', 'b', 'x', 'd']],
        [[1, 1], 2, ['a', 'b', 'c', 'x']],
        [[1, 1], 3, ['a', 'b', 'c', 'd', 'x', 'f', 'g', 'h', 'i']],
      ])('%j', function (coordinates, itemInRow, array2D) {
        expect(getItem(itemInRow)(array2D)(coordinates)).toStrictEqual('x')
      });
    });

    describe('Get coordinates from index ', () => {
      it.each<[number, number, point]>([
        [0, 2, [0, 0]],
        [1, 2, [1, 0]],
        [2, 2, [0, 1]],
        [3, 2, [1, 1]],
      ])('%j', function (index, itemInRow, coordinates) {
        expect(calculateCoordinates(itemInRow)(index)).toStrictEqual(coordinates)
      });
    });
  });
});
