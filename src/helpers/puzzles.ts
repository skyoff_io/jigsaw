import {puzzle} from "../types";
import {calculateCoordinates} from "./array2D";

export const createPuzzleByIndex = (horizontalCount: number) => (size: {width: number, height: number}) => (index:number) => ({
  id: index,
  dx: calculateCoordinates(horizontalCount)(index)[0] * size.width,
  dy: calculateCoordinates(horizontalCount)(index)[1] * size.height,
});

export const getPuzzleById = (puzzles: puzzle[]) => (id: number):puzzle => puzzles.find(puzzle => puzzle.id === id) as puzzle;
