import {loadImageResult} from "../types";

/**
 * Create image from url
 * Side effect function!
 * @param {string} url
 */
const createImage = async (url: string): Promise<HTMLImageElement> => new Promise((resolve, reject) => {
  const image = new Image();
  image.src = url;
  image.onload = () => resolve(image);
  image.onerror = reject;
});

/**
 * Load image
 * @param url
 */
export const loadImage = async (url: string): Promise<loadImageResult> => {
  const result: loadImageResult = {};
  try {
    result.image = await createImage(url);
  } catch (e) {
    result.error = "We can't download this image, url is correct?";
  }

  return result;
};
