import React from "react";
import {Button, Col, Image} from "react-bootstrap";

export const ExampleGame: React.FC<{ selectGame: (game: [string, string]) => void }> = (props) => {

  const games: [string, string][] = [
    ['https://www.jigsawplanet.com/i/b340ec00900b000800442f0c0a417a7aab/1024/jp.jpg', 'https://s02.jigsawplanet.com/i/b340ec00900b000800442f0c0a417a7aab/160/jp.jpg'],
    ['https://s02.jigsawplanet.com/i/f9eea400be01800700bdbf2f284e8e759e/1024/jp.jpg', 'https://s02.jigsawplanet.com/i/f9eea400be01800700bdbf2f284e8e759e/160/jp.jpg'],
    ['https://www.jigsawplanet.com/i/c0c570007a0c8007001f6849de0c0486fd/1024/jp.jpg', 'https://www.jigsawplanet.com/i/c0c570007a0c8007001f6849de0c0486fd/160/jp.jpg'],
  ];

  return (
    <React.Fragment>
      {
        games.map((ele, index) => (
          <Col key={index} xs={2}>
            <Button onClick={() => props.selectGame(ele)} variant="link">
              <Image src={ele[1]} rounded fluid/>
            </Button>
          </Col>
        ))
      }
    </React.Fragment>
  )
};
