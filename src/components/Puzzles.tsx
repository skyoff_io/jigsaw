import React from 'react';
import {GameState} from "../reducers/game";
import {Puzzle} from "./Puzzle";
import {datasetPuzzle, point} from "../types";
import {getPuzzleById} from "../helpers/puzzles";
import {calculateCoordinates} from "../helpers";
import {DragPuzzle} from "./DragPuzzle";

const positionToPoint = (game: GameState) => (position: point) => [Math.floor(position[0] / game.size.widthPuzzle), Math.floor(position[1] / game.size.heightPuzzle)];

export const Puzzles: React.FC<{ game: GameState, movePuzzle: any }> = (props) => {

  const isWithinGameX = (left: number) => left >= 0 && left <= props.game.size.widthPuzzle * props.game.size.horizontalCount;
  const isWithinGameY = (top: number) => top >= 0 && top <= props.game.size.heightPuzzle * props.game.size.verticalCount;
  const isWithinGame = (position: point) => isWithinGameX(position[0]) && isWithinGameY(position[1]);

  const endMovePuzzle = (data: datasetPuzzle) => (position: point): boolean => {
    return isWithinGame(position) && (props.movePuzzle({
      from: data.point,
      to: positionToPoint(props.game)(position)
    }) || true);
  };

  const getPuzzle = getPuzzleById(props.game.puzzles);
  const getCoordinates = calculateCoordinates(props.game.size.horizontalCount);
  const getDatasetPuzzle = (puzzleId: number, index: number) => ({
    puzzle: getPuzzle(puzzleId),
    point: getCoordinates(index)
  });
  const createDataset = (): datasetPuzzle[] => props.game.structurePuzzles.map<datasetPuzzle>(getDatasetPuzzle);

  return (
    <div style={{
      background: '#f6f6f6',
      position: 'relative',
      userSelect: "none",
      width: props.game.size.horizontalCount * props.game.size.widthPuzzle,
      height: props.game.size.verticalCount * props.game.size.heightPuzzle
    }}>
      {createDataset().map((data: datasetPuzzle) =>
        <DragPuzzle
          key={data.puzzle.id}
          data={data}
          game={props.game}
          onMovePuzzleEnd={endMovePuzzle(data)}
        >
          <Puzzle
            data={data}
            game={props.game}
          />
        </DragPuzzle>
      )}
    </div>
  )
};
