import React, {useEffect, useRef} from 'react';
import {datasetPuzzle, point} from "../types";
import {GameState} from "../reducers/game";

export interface PuzzleProps {
  data: datasetPuzzle;
  game: GameState;
  onMovePuzzle?: (position: point) => void;
  onMovePuzzleEnd?: (position: point) => void;
}

export const Puzzle: React.FC<PuzzleProps> = (props) => {
  const canvasRef = useRef(null);

  useEffect(() => {
    if(props.game.image) {
      // @ts-ignore
      const ctx = canvasRef.current.getContext('2d');
      ctx.drawImage(props.game.image, props.data.puzzle.dx * -1 , props.data.puzzle.dy * -1);
    }
  });

  return (
    <canvas ref={canvasRef} width={props.game.size.widthPuzzle} height={props.game.size.heightPuzzle} />
  )
};
