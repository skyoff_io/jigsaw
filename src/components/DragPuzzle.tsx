import React, {useEffect, useRef, useState} from 'react';
import {datasetPuzzle, point} from "../types";
import {GameState} from "../reducers/game";

export interface PuzzleProps {
  data: datasetPuzzle;
  game: GameState;
  onMovePuzzle?: (position: point) => void;
  onMovePuzzleEnd?: (position: point) => void;
}

export const DragPuzzle: React.FC<PuzzleProps> = (props) => {
  const divRef = useRef(null);
  const [isDrag, setDrag] = useState(false);
  const resetPositionX = () => props.data.point[0] * props.game.size.widthPuzzle;
  const resetPositionY = () => props.data.point[1] * props.game.size.heightPuzzle;
  const [left, setLeft] = useState(resetPositionX());
  const [top, setTop] = useState(resetPositionY());

  const resetPosition = () => {
    setLeft(resetPositionX());
    setTop(resetPositionY())
  };

  useEffect(() => {
    resetPosition();
  }, [props.data.point]);

  let tmpLeft = left;
  let tmpTop = top;

  const dragPuzzle = (event: any) => {
    // @ts-ignore
    const deltaTop = event.clientY - divRef.current.offsetTop;
    // @ts-ignore
    const deltaLeft = event.clientX - divRef.current.offsetLeft;
    const deltaMouseTop = event.nativeEvent.offsetY;
    const deltaMouseLeft = event.nativeEvent.offsetX;

    setDrag(true);

    const movePuzzle = (event: any) => {
      tmpLeft = event.x - deltaLeft;
      tmpTop = event.y - deltaTop;

      props.onMovePuzzle && props.onMovePuzzle([tmpLeft, tmpTop]);
      setLeft(event.x - deltaLeft);
      setTop(event.y - deltaTop)
    };

    const releasePuzzle = () => {
      (props.onMovePuzzleEnd && props.onMovePuzzleEnd([tmpLeft + deltaMouseLeft, tmpTop + deltaMouseTop])) || resetPosition();
      setDrag(false);
      document.onmousemove = null;
      document.onmouseup = null;
    };

    document.onmousemove = movePuzzle;
    document.onmouseup = releasePuzzle;
  };

  return (
    <div
      ref={divRef}
      onMouseDown={dragPuzzle}
      style={{
        zIndex: isDrag ? 1000 : 1,
        boxShadow: isDrag ? "0px 10px 13px -7px rgba(0,0,0,0.5)" : "none",
        width: props.game.size.widthPuzzle,
        height: props.game.size.heightPuzzle,
        position: 'absolute',
        left,
        top,
      }}>
      {props.children}
    </div>
  )
};
