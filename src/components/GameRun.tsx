import React from 'react';
import {GameState} from "../reducers/game";
import {Puzzles} from "./Puzzles";
import {Alert} from "react-bootstrap";
import {resetGameInterface, movePuzzleInterface} from "../actions";

export const GameRun: React.FC<{ game: GameState, movePuzzle: movePuzzleInterface, resetGame:resetGameInterface }> = (props) => {

  const renderMessage = () => {
    return (
      <React.Fragment>
        {props.game.isEnd && (<Alert  variant='success'>Well done!  <Alert.Link onClick={() => props.resetGame()}>Another one?</Alert.Link></Alert>)}
      </React.Fragment>
    )
  };

  return (
    <React.Fragment>
      {renderMessage()}
      <Puzzles game={props.game} movePuzzle={props.movePuzzle}/>
    </React.Fragment>
  )
};
