import React, {useState} from "react";
import {jigsawGenerator} from "../helpers/jigsawGenerator";
import {Alert, Button, Col, Container, FormControl, InputGroup, Row, Spinner} from "react-bootstrap";
import * as Config from '../config';
import {ExampleGame} from './';

export const GameSetup: React.FC<{ initGame: any }> = (props) => {
  const [url, setUrl] = useState("");
  const [spinner, setSpinner] = useState(false);
  const [error, _setError] = useState();

  const setError = (error: string) => {
    _setError(error);
    return true;
  };

  const load = async (url: string) => {
    setSpinner(true);
    const result = await jigsawGenerator(Config.puzzle.targetSize)(Config.image.min)(Config.image.max)(url) as any;
    setSpinner(false);

    (result.error && setError(result.error)) || props.initGame(result);
  };

  const renderSpinner = () => (
    <div style={{position: 'fixed', zIndex: 1000, top: 0, left: 0, right: 0, bottom: 0, display: "flex", justifyContent: "center", alignItems: "center", background: "rgba(255,255,255,0.5)"}}>
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
    </div>);

  const renderSetup = () => (
    <Container>
      <Row className="justify-content-md-center">
        <Col xs={6}>
          <p>Enter an image URL to generate jigsaw puzzle:</p>
          <InputGroup className="mb-3">
            <FormControl
              placeholder="http://"
              onChange={(event: any) => setUrl(event.target.value)}
            />
            <InputGroup.Append>
              <Button variant="primary" type="submit" onClick={() => load(url)}>Generate</Button>
            </InputGroup.Append>
          </InputGroup>
          {error && (<Alert variant='danger'>{error}</Alert>)}
        </Col>
      </Row>

      <Row className="justify-content-md-center">
        <ExampleGame selectGame={(game => load(game[0]))}/>
      </Row>
    </Container>
  );

  return (
    <React.Fragment>
      {spinner && renderSpinner()}
      {renderSetup()}
    </React.Fragment>
  );
};
