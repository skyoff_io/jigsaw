import puzzle from "./puzzle";
import image from "./image";
require('dotenv').config();

export {
  image,
  puzzle
}
