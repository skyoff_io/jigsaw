const envVars:any = process.env;

const image = {
  min: envVars.REACT_APP_PUZZLE_SIZE_MIN || 260,
  max: envVars.REACT_APP_PUZZLE_SIZE_MAX || 1024,
};

export default image;
