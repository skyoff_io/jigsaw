const envVars: any = process.env;

const puzzle = {
  targetSize: envVars.REACT_APP_PUZZLE_SIZE_TARGET || 130,
};

export default puzzle;
