import {INIT_GAME, MOVE_PUZZLE} from '../constants';
import game, {GameState} from './game';
import {scenario1, scenario2} from '../mock';
import {point} from "../types";

describe('Reducers', () => {
  describe('Game', () => {
    describe('Init new game', () => {
      const payload: GameState = {
        isStart: true,
        isEnd: false,
        size: {
          horizontalCount: 0,
          verticalCount: 0,
          heightPuzzle: 0,
          widthPuzzle: 0,
        },
        puzzles: [],
        structurePuzzles: []
      };

      const state = game(scenario1, {type: INIT_GAME, payload});

      it('should set property false', function () {
        expect(state.isEnd).toBe(payload.isEnd);
      });

      it('should set new structurePuzzles', function () {
        expect(state.structurePuzzles).toStrictEqual(payload.structurePuzzles)
      });
    });

    describe('Move puzzle ', () => {
      describe.each<[point, point, number, number]>(
        [
          [[1, 0], [0, 0], 0, 1],
          [[1, 1], [0, 1], 4, 3],
          [[1, 2], [1, 1], 7, 4]
        ]
      )('from %j to %j ', (from, to, indexA, indexB) => {
        const payload = [{from, to}];
        const a = scenario1.structurePuzzles[indexA];
        const b = scenario1.structurePuzzles[indexB];

        const state = game(scenario1, {type: MOVE_PUZZLE, payload});

        it('should switch puzzle A"', function () {
          expect(state.structurePuzzles[indexA]).toBe(b)
        });

        it('should switch puzzle B', function () {
          expect(state.structurePuzzles[indexB]).toBe(a)
        });

        it('game is not over', function () {
          expect(state.isEnd).toBeFalsy();
        });
      });
    });

    describe('Move puzzle to end game', () => {
      const payload = [{from: [1, 0], to: [0, 0]}];

      const state = game(scenario2, {type: MOVE_PUZZLE, payload});

      it('game is over', function () {
        expect(state.isEnd).toBeTruthy();
      });
    });
  });
});
