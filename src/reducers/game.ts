import {actionState, gameStatePuzzles, point, puzzle} from "../types";
import {INIT_GAME, MOVE_PUZZLE, RESET} from "../constants";
import {addItem, getItem} from "../helpers";

export interface GameState {
  image?: HTMLImageElement,
  isStart: boolean;
  isEnd: boolean;
  size: {
    verticalCount: number,
    horizontalCount: number,
    heightPuzzle: number,
    widthPuzzle: number,
  }
  puzzles: puzzle[],
  structurePuzzles: gameStatePuzzles
}

const initialState: GameState = {
  isStart: false,
  isEnd: false,
  size: {
    verticalCount: 0,
    horizontalCount:0,
    heightPuzzle: 0,
    widthPuzzle: 0,
  },
  puzzles: [],
  structurePuzzles: [],
};

export default function game(state = initialState, action: actionState<any>): GameState {
  switch (action.type) {
    case RESET:
      return initialState;
    case INIT_GAME:
      return action.payload;
    case MOVE_PUZZLE:
      return gameIsOver({
        ...state,
        structurePuzzles: movePuzzles(state)(action.payload)
      });
    default:
      return state
  }
};


const movePuzzles = (state: GameState) => (task: { from: point, to: point }[]): gameStatePuzzles => {
  return task.reduce<any>(movePuzzle(state), state.structurePuzzles);
};

const movePuzzle = (state: GameState) => (puzzles: [][], task: { from: point, to: point }): gameStatePuzzles => {
  const add = addItem(state.size.horizontalCount);
  const get = getItem(state.size.horizontalCount);
  const move = (array: any) => (from: point) => (to: point) => add(array)(to)(get(puzzles)(from));
  return move(move(puzzles)(task.from)(task.to))(task.to)(task.from) as gameStatePuzzles;
};

const gameIsOver = (state: GameState): GameState => ({
  ...state,
  isEnd: state.structurePuzzles.map((item, index) => item === index).every(item => item),
});

