import {applyMiddleware, combineReducers, createStore} from 'redux'
import game from './game'
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  game,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store
