import React from 'react';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import Game from './containers/Game';
import store from './reducers';
require('dotenv').config();

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <div className="App">
        <Game />
      </div>
    </Provider>
  );
};

export default App;
