import {GameState} from "../reducers/game";
import {Dispatch} from "redux";
import {INIT_GAME} from "../constants";

export interface initGameInterface {
  (payload: GameState): any;
}

export const initGame: initGameInterface = (payload) => (dispatch: Dispatch) => {
  dispatch({type: INIT_GAME, payload});
};
