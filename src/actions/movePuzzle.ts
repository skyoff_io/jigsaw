import {Dispatch} from "redux";
import {movePuzzleData} from "../types";
import {MOVE_PUZZLE} from "../constants";

export interface movePuzzleInterface {
  (payload: movePuzzleData | movePuzzleData[]): any;
}

export const movePuzzle: movePuzzleInterface = (payload) => (dispatch: Dispatch) => {
  dispatch({type: MOVE_PUZZLE, payload: (Array.isArray(payload) && payload) || [payload]});
};
