import {Dispatch} from "redux";
import {RESET} from "../constants";

export interface resetGameInterface {
  (): any;
}

export const resetGame: resetGameInterface = () => (dispatch: Dispatch) => {
  dispatch({type: RESET});
};
